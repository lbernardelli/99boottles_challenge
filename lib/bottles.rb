class Bottles
  def song
    verses(99, 0)
  end

  def verses(starting, ending)
    starting.downto(ending).map { |i| verse(i) }.join("\n")
  end

  def verse(number)
    container = ContainerFactory.for(number)
    successor = container.successor

    "#{container.quantity.capitalize} #{container.name} of beer on the wall, " +
        "#{container.quantity} #{container.name} of beer.\n" +
        "#{container.action}, " +
        "#{successor.quantity} #{successor.name} of beer on the wall.\n"
  end
end

class ContainerFactory
  def self.for(number)
    begin
      return Object.const_get("ContainerNumber#{number}").new(number)
    rescue NameError
      return ContainerNumber.new(number)
    end
  end
end

class ContainerNumber
  def initialize(number)
    @number = number
  end

  def successor
    ContainerFactory.for(@number-1)
  end

  def quantity
    @number.to_s
  end

  def action
    "Take #{pronoun} down and pass it around"
  end

  def name
    "bottles"
  end

  def pronoun
    "one"
  end
end

class ContainerNumber0 < ContainerNumber
  def quantity
    "no more"
  end

  def action
    "Go to the store and buy some more"
  end

  def successor
    ContainerFactory.for(99)
  end
end

class ContainerNumber1 < ContainerNumber
  def name
    "bottle"
  end

  def pronoun
    "it"
  end
end

class ContainerNumber6 < ContainerNumber
  def quantity
    1.to_s
  end

  def name
    "six-pack"
  end
end